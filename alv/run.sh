#!/bin/bash

session_id=$1
url=$2
username=$3
password=$4

python3 /usr/local/bin/download_previous_fll.py $session_id $url $username $password

find /baseline -type f -name "*fll_bin.nii.gz" -exec cp -v {} /tmp/fll_bin0.nii.gz \;
find /work -type f -name "*fll_bin.nii.gz" -exec cp -v {} /tmp/fll_bin1.nii.gz \;

find /work -type f -name "*I_stdmaskbrain_pve_0.nii.gz" -exec cp -v {} /tmp/I_stdmaskbrain_pve_0.nii.gz \;
find /work -type f -name "*I_stdmaskbrain_pve_0_segvent.nii.gz" -exec cp -v {} /tmp/I_stdmaskbrain_pve_0_segvent.nii.gz \;

/bin/run
