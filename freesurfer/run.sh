#!/bin/bash

input=$1
output=$2

dcmunpack -src $input -scanonly scans.log

mkdir /tmp/xnat

for scan in $(cat scans.log | awk '{print $9}'); do
    echo $scan;
    recon-all -all -s xnat -sd $output -i $scan 
    rm -f /output/fsaverage
    mv /opt/freesurfer/subjects/fsaverage $output/
done