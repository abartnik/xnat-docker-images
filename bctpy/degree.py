import bct
import numpy as np
import os
import csv
import begin


@begin.start
def main(infile: "Input functional connectome",
         output: "File to write degree matrix to"):
    connectome = np.recfromtxt(infile, dtype=float, delimiter=',')
    degrees = bct.degree.degrees_dir(connectome)[-1]
    labels = np.recfromtxt('/opt/bctpy/atlas_labels.txt', dtype='str')
    degree_dict = { labels[i]: degrees[i] for i in range(len(labels)) }
    
    with open(output, 'w') as outfile:
        outfile.write("Label, Degree\n") 
        for label, degree in degree_dict.items(): 
            outfile.write(f"{label}, {degree}\n") 
