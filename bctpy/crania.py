import bct
import os
import numpy as np
import json
import begin


@begin.start
def main(measure):

    if measure == 'correlation':
        path = '/input/FUNCTIONAL_CONNECTOMES_CORRELATION'
    elif measure == 'sparse':
        path ='/input/FUNCTIONAL_CONNECTOMES_SPARSE'
    
    network_files = []
    for csv in os.listdir(path):
        if csv.split('.')[-1] == 'csv' and 'long_fc' not in csv:
            network_files.append(csv)

    networks = {}
    for network in network_files:

        if 'long' in network:
            continue
        
        if measure == 'correlation':
            network_name = network.split('_pearson')[0]
        elif measure == 'sparse':
            network_name = network.split('_sparse')[0]
        network_mat = np.recfromtxt( os.path.join(path, network), dtype=float, delimiter=',')
        networks[network_name] = np.absolute(network_mat)

    def graph_theory_measures(network):
        measures = {
                    'mean': np.mean(network).item(),
                    'efficiency':  bct.efficiency_wei(network), 
                    'assortativity': bct.assortativity_wei(network), 
                    'transitivity': float(bct.transitivity_wu(network))
                    }
        return(measures)

    measures = {}
    for network_name, network in networks.items():
        measures[network_name] = (graph_theory_measures(network))
    
    with open(f'/output/{measure}_graph_theory_measures.json', 'w') as outjson:
        json.dump(measures, outjson, indent=2)
