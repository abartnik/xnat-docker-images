#!/bin/bash

t1=$1
flair=$2
fll=$3

find "$t1" -type f -name "*nii" -exec gzip {} \;
find "$t1" -type f -name "*nii.gz" -exec cp -v {} /work/t1.nii.gz \;

find "$flair" -type f -name "*nii" -exec gzip {} \;
find "$flair" -type f -name "*nii.gz" -exec cp -v {} /work/flair.nii.gz \;

find "$fll" -type f -name "*nii" -exec gzip {} \;
find "$fll" -type f -name "*nii.gz" -exec cp -v {} /work/fll.nii.gz \;

N4BiasFieldCorrection \
    --image-dimensionality 3 \
    --input-image /work/t1.nii.gz \
    --output /work/t1_n4.nii.gz \
    --shrink-factor 4 \
    --convergence [50x50x50x50,0.0000001] \
    --bspline-fitting [200]

bet /work/t1_n4.nii.gz /work/t1_bet -s -B -f 0.1

fast -v -N /work/t1_bet.nii.gz

flirt -v \
    -in /work/flair.nii.gz \
    -ref /work/t1_n4.nii.gz \
    -out /work/flair_in_t1.nii.gz \
    -omat /work/flair_in_t1.mat \
    -dof 6

flirt -v \
    -in /work/fll.nii.gz \
    -ref /work/t1_n4.nii.gz \
    -out /work/fll_in_t1.nii.gz \
    -init /work/flair_in_t1.mat \
    -applyxfm \
    -interp nearestneighbour

fslmaths /work/t1_bet_pve_2.nii.gz -thr 0.5 /work/t1_bet_pve_2_thr.nii.gz
fslmaths /work/t1_bet_pve_2_thr.nii.gz -bin /work/t1_wmmask_bin.nii.gz
fslmaths /work/fll_in_t1.nii.gz -thr 0.01 /work/fll_thr.nii.gzs
fslmaths /work/fll_thr.nii.gz -bin /work/fll_bin.nii.gz
fslmaths /work/fll_bin.nii.gz -mas /work/t1_wmmask_bin.nii.gz /work/fll_bin_masked.nii.gz

lesion_filling \
    -i /work/t1_n4.nii.gz \
    -l /work/fll_bin_masked.nii.gz \
    -w /work/t1_wmmask_bin.nii.gz \
    -o /work/t1_inpainted.nii.gz

sienax /work/t1_inpainted.nii.gz -r -d -P /work/t1_bet.nii.gz /work/t1_bet_skull.nii.gz
rsync -av /work/t1_inpainted_sienax/ /output --progress
