#!/bin/bash

t1=$1

url=$2
username=$3
password=$4
session_id=$5


find "$t1" -type f -name "*nii" -exec gzip {} \;
find "$t1" -type f -name "*nii.gz" -exec cp -v {} /work/t1.nii.gz \;

N4BiasFieldCorrection \
    --image-dimensionality 3 \
    --input-image /work/t1.nii.gz \
    --output /work/t1_n4.nii.gz \
    --shrink-factor 4 \
    --convergence [50x50x50x50,0.0000001] \
    --bspline-fitting [200]

bet /work/t1_n4.nii.gz /work/t1_bet -s -f 0.4 -B

get_available_flair.py $url $username $password $session_id
find /tmp/flair -type f -name "flair_low.nii.gz" -exec cp -v {} /tmp/flair/flair.nii.gz \;
find /tmp/fll -type f -name "*nii.gz" -exec cp -v {} /tmp/fll/fll.nii.gz \;

if [ -f /tmp/flair/flair.nii.gz ] ; then 
    t1_lesion_inpainting.sh /work/t1_n4.nii.gz /tmp/flair/flair.nii.gz /tmp/fll/fll.nii.gz
    cp -v /work/t1_inpainted.nii.gz /work/t1.nii.gz
fi

sienax /work/t1.nii.gz -r -d -P /work/t1_bet.nii.gz /work/t1_bet_skull.nii.gz
rsync -av /work/t1_sienax/ /output --progress
