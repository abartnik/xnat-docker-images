import begin
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker


def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def generate_assessor_xml(pbvc, xnat_host, project_name, session_id, session_label):

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = '{}_SIENA'.format(session_id)
    assessorLabel = '{}_SIENA'.format(session_label)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E('Siena', assessorTitleAttributesDict,
                    E(ns('xnat', 'date'), date),
                    E(ns('xnat', 'imageSession_ID'), session_id),
                    E(ns('crania', 'percentBrainVolumeChange'), str(pbvc))
                )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    with open('/output/percent_brain_volume_change.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(xnat_host,
        project_name,
        session_id,
        session_label):

    with open('/input/SIENA/report.siena', 'r') as results:
        siena_results = [ line.strip() for line in results.readlines() ]

    raw_pbvc = siena_results[-1].split(' ')[1]
    if '-.' in raw_pbvc:
        pbvc = float(raw_pbvc[:1] + "0" + raw_pbvc[1:])
    elif '0' not in raw_pbvc:
        pbvc = float("0" + raw_pbvc)
    else:
        pbvc = float(raw_pbvc)
    print(pbvc)

    generate_assessor_xml(pbvc, xnat_host, project_name, session_id, session_label)
