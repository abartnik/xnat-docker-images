#!/usr/bin/env python

import begin
import xnat


def find_previous_sessions(session_id, url, username, password):

    past_sessions = {}
    with xnat.connect(url, user=username, password=password) as session:
        subject = session.experiments[session_id].subject.fulluri.split('/')[-1]
        subject_data = session.subjects[subject]
        
        for exam in subject_data.experiments.data:
            exam_data = subject_data.experiments[exam]
            if exam != session_id:
                for scan in exam_data.scans.data:
                    for resource in exam_data.scans[scan].resources.data:
                        if exam_data.scans[scan].resources[resource].data['label'] == 'SIENAX':
                            past_sessions[exam_data.date] = exam

    first = past_sessions[min(past_sessions.keys())]
    if len(past_sessions) > 1:
        most_recent = past_sessions[max(past_sessions.keys())]
        return(first, most_recent)
    else:
        return(first, None)

@begin.start
def main(session_id, url, username, password):

    first, most_recent = find_previous_sessions(session_id, url, username, password)
    with xnat.connect(url, user=username, password=password) as session:
        for scan in session.experiments[first].scans:
            for resource in session.experiments[first].scans[scan].resources.data:
                if session.experiments[first].scans[scan].resources[resource].data['label'] == 'SIENAX':
                    session.experiments[first].scans[scan].resources[resource].download_dir('/baseline')
