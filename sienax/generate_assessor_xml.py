import begin
import csv
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker
from bs4 import BeautifulSoup as bs
import re


def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def generate_assessor_xml(sienax_results, xnat_host, project_name, session_id, session_label, scan_id):

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = '{}_SIENAX'.format(session_id, scan_id)
    assessorLabel = '{}_SIENAX'.format(session_label, scan_id)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E('Sienax', assessorTitleAttributesDict,
                    E(ns('xnat', 'date'), date),
                    E(ns('xnat', 'imageSession_ID'), session_id),
                    E(ns('crania', 'peripheralGreyMatterVolume'), 
                    E(ns('crania', 'normalizedPgreyVolume'), sienax_results['pgrey']['normalized']), 
                    E(ns('crania', 'nonnormalizedPgreyVolume'), sienax_results['pgrey']['nonnormalized'])
                    ),
                    E(ns('crania', 'ventricularCSFVolume'),
                    E(ns('crania', 'normalizedVcsfVolume'), sienax_results['vcsf']['normalized']),
                    E(ns('crania', 'nonnormalizedVcsfVolume'), sienax_results['vcsf']['nonnormalized'])
                    ),
                    E(ns('crania', 'cerebrospinalFluidVolume'), 
                    E(ns('crania', 'normalizedCerebrospinalFluidVolume'), sienax_results['CSF']['normalized']),
                    E(ns('crania', 'nonnormalizedCerebrospinalFluidVolume'), sienax_results['CSF']['nonnormalized'])
                    ),
                    E(ns('crania', 'greyMatterVolume'), 
                    E(ns('crania', 'normalizedGreyMatterVolume'), sienax_results['GREY']['normalized']),
                    E(ns('crania', 'nonnormalizedGreyMatterVolume'), sienax_results['GREY']['nonnormalized'])
                    ),
                    E(ns('crania', 'whiteMatterVolume'), 
                    E(ns('crania', 'normalizedWhiteMatterVolume'), sienax_results['WHITE']['normalized']),
                    E(ns('crania', 'nonnormalizedWhiteMatterVolume'), sienax_results['WHITE']['nonnormalized'])
                    ),
                    E(ns('crania', 'wholeBrainVolume'), 
                    E(ns('crania', 'normalizedWholeBrainVolume'), sienax_results['BRAIN']['normalized']),
                    E(ns('crania', 'nonnormalizedWholeBrainVolume'), sienax_results['BRAIN']['nonnormalized'])
                    )
                )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    with open('/output/sienax.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(xnat_host,  
        username, 
        password,
        project_name,
        session_id,
        session_label,
        scan_id):

    with open('/input/SIENAX/report.html', 'r', encoding='utf-8') as infile:
        html = infile.read()
        soup = bs(html)
    sienax_report = soup.find_all('pre')[0]

    sienax_results = {}
    for i in range(2,8):
        row = sienax_report.text.split('\n')[i]
        row_list = re.sub('\s+',' ', row).split(' ')
        sienax_results[row_list[0]] = {'normalized': row_list[1], 'nonnormalized': row_list[2]}

    with open('/output/sienax.csv', 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=list(sienax_results.keys()))
        writer.writeheader()
        writer.writerow(sienax_results)

    generate_assessor_xml(sienax_results, xnat_host, project_name, session_id, session_label, scan_id)
