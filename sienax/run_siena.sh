#!/bin/bash

session_id=$1
url=$2
username=$3
password=$4

/bin/download_baseline_sienax.py $session_id $url $username $password

mkdir -p /work/baseline /work/followup

find /baseline -type f -name "*I.nii.gz" -exec mv -v {} /work/baseline/I.nii.gz \;

if [ ! -f /work/baseline/I.nii.gz ]; then
    exit 1
fi

find /baseline -type f -name "*I_brain.nii.gz" -exec mv -v {} /work/baseline/I_brain.nii.gz \;
find /baseline -type f -name "*I_brain_skull.nii.gz" -exec mv -v {} /work/baseline/I_brain_skull.nii.gz \;

find /followup -type f -name "*I.nii.gz" -exec cp -v {} /work/followup/I.nii.gz \;
find /followup -type f -name "*I_brain.nii.gz" -exec cp -v {} /work/followup/I_brain.nii.gz \;
find /followup -type f -name "*I_brain_skull.nii.gz" -exec cp -v {} /work/followup/I_brain_skull.nii.gz \;

siena \
    baseline/I.nii.gz \
    followup/I.nii.gz \
    -P baseline/I_brain.nii.gz baseline/I_brain_skull.nii.gz \
        followup/I_brain.nii.gz followup/I_brain_skull.nii.gz \
    -o /output
