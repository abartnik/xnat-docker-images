import xnat
from scan_classifier.scan_classifier import (read_dcm_header, read_bids_sidecar, 
                                             format_input_for_model, predict_scan_type, 
                                             model)
from scan_classifier.ScanType import ScanType
from nipype.interfaces.dcm2nii import Dcm2niix
import nibabel as nb
from pathlib import Path
from collections import namedtuple
import begin


@begin.start
def main(xnat_url, xnat_user, xnat_password, exam_id):
    
    with xnat.connect(xnat_url, user = xnat_user, password = xnat_password) as session:
        exam_data = session.experiments[exam_id]

        scan_types = {}
        for scan_id, scan_data in exam_data.scans.items():
            try:
                int(scan_id)
            except ValueError:
                print(f"{scan_id} filtered out due to series number heuristic")
                continue
            
            series_description, uid = scan_data.series_description, scan_data.uid
            if "local" in series_description.lower() or \
                "fuji" in series_description.lower() or \
                "report" in series_description.lower() or \
                "asset" in series_description.lower() or \
                "cal" in series_description.lower() or \
                "scout" in series_description.lower() or \
                "phant" in series_description.lower() or \
                "adc" in series_description.lower():
                print(f"{scan_id} filtered out due to series description heuristic")
                continue

            print(scan_id, scan_data)
            header = scan_data.read_dicom()
            metadata = {}
            params = ['EchoTime', 'InversionTime', 'RepetitionTime', 'DiffusionBValue', 
                    'EchoTrainLength', 'FlipAngle']
            for param in params:
                if header.get(param) != None:
                    metadata[param] = header.get(param)
                else:
                    metadata[param] = 0.0
            if header.get('ScanningSequence') != None:
                metadata['ScanningSequence'] = header.get('ScanningSequence')
            else:
                metadata['ScanningSequence'] = 'RM'
            model_inputs = format_input_for_model(metadata)
            pred_scan_type = predict_scan_type(model, model_inputs)
            
            if pred_scan_type == ScanType.fmap and 'se' in series_description.lower():
                scan_type = "T1SE"
            if pred_scan_type == ScanType.T2star and 't1' in series_description.lower():
                scan_type = "T1w"
            if pred_scan_type == ScanType.PD and 't1' in series_description.lower():
                scan_type = "T1w"
            elif pred_scan_type == ScanType.FLAIR and 'dir' in series_description.lower():
                scan_type = "DIR"
            else:
                scan_type = pred_scan_type.name
            print(scan_type)

            download_dir = Path(f"/tmp/{exam_id}/{scan_id}")
            nifti_dir = download_dir / "nifti"
            if not download_dir.is_dir():
                download_dir.mkdir(parents = True)
            if not nifti_dir.is_dir():
                nifti_dir.mkdir(parents = True)

            scan_data.download_dir(download_dir)
            dcmdir = str(list(download_dir.rglob("*DICOM/files"))[0])
            
            converter = Dcm2niix()
            converter.inputs.source_dir = dcmdir
            converter.inputs.output_dir = nifti_dir
            converter.run()
            
            nifti_file = str(list(nifti_dir.glob("*nii.gz"))[0])
            img = nb.load(nifti_file)
            pixdim3 = img.header['pixdim'][3]
            
            ScanQuality = namedtuple("ScanQuality", "scan_id z")
            scan_quality = ScanQuality(scan_id, pixdim3)
            try:
                scan_types[scan_type].append(scan_quality)
            except KeyError:
                scan_types[scan_type] = []
                scan_types[scan_type].append(scan_quality)

        for scan_type, scans in scan_types.items():
            if len(scans) > 1:
                best_slice_thickness = min([ scan.z for scan in scans ])
                highest_series_number = max([ scan.scan_id for scan in scans ])
                candidate_scan = []
                for scan in scans:
                    if scan.z == best_slice_thickness:
                        candidate_scan.append(scan)
                if len(candidate_scan) > 1:
                    for scan in scans:
                        if scan.scan_id == highest_series_number:
                            candidate_scan = [scan]
                
            else:
                candidate_scan = [scans[0]]
            
            exam_data.scans[candidate_scan[0].scan_id].type = scan_type
            print(exam_data.scans[candidate_scan[0].scan_id], exam_data.scans[candidate_scan[0].scan_id].type)
