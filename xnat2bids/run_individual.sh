#!/bin/bash

session_id=$1
xnat_url=$2
xnat_user=$3
xnat_passwd=$4

echo $session_id
subject_id=$(get_xnat_subject_id.py $session_id $xnat_url $xnat_user $xnat_passwd)
echo $subject_id

if [ ${#session_id} -lt 1 ]; then
    session_id="01"
fi

mv /bids/sub- /bids/sub-$subject_id
mv /bids/sub-$subject_id/ses- /bids/sub-$subject_id/ses-$session_id
echo "sub-$session_id" >> /bids/participant.tsv

mkdir /tmp/niftis
if [[ ! -n $(find /input -type d -name "*NIFTI") ]]; then
    dcm2niix -z y -b y -f %d_%x_%z_%s -o /tmp/niftis /input
else
    find /input -type f -wholename "**/NIFTI/**nii.gz" -exec cp -v {} /tmp/niftis \;
    find /input -type f -wholename "**/NIFTI/**json" -exec cp -v {} /tmp/niftis \;
fi

xnat2bids_session.py $subject_id $session_id

tree /bids

rsync -av /bids/ /output --progress
# mriqc /bids /output participant --no-sub
