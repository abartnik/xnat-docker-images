#!/usr/bin/env python3

import os
import shutil
from glob import glob
import numpy as np
from scan_classifier.scan_classifier import read_dcm_header, read_bids_sidecar, format_input_for_model, predict_scan_type, model

if os.path.isdir('/bids/sub-01'):
    shutil.rmtree('/bids/sub-01')
sessions = []

for root, dirs, files in os.walk('/input'):
    # print(root)
    
    try:
        session = root.split('/')[2]
        sessions.append(session)
        
    except IndexError:
        continue

scan_types = {}
for session in np.unique(sessions):
    print(session)
    try:
        session_scans = {}
        for scan in os.listdir(f"/input/{session}/SCANS/"):
            try:
                scan_contents = glob(f"/input/{session}/SCANS/{scan}/DICOM/*dcm")[0]
                metadata = read_dcm_header(scan_contents)
            except FileNotFoundError:
                scan_contents = glob(f"/input/{session}/SCANS/{scan}/NIFTI/*json")[0]
                metadata = read_bids_sidecar(scan_contents)
            except IndexError:
                pass
            except FileNotFoundError:
                print(f"No DICOM or NIFTI for {session}/{scan}")
                continue
            
            model_inputs = format_input_for_model(metadata)
            scan_type = predict_scan_type(model, model_inputs)

            session_scans[scan_type] = f"/input/{session}/SCANS/{scan}"

            print(f"{session}/{scan}: {scan_type}")
        print(" ")

    except FileNotFoundError:
        pass

    scan_types[session] = session_scans
print(scan_types)
