#!/usr/bin/env python3

import xnat
import sys


try:
    exam_id = sys.argv[1]
    xnat_url = sys.argv[2]
    xnat_user = sys.argv[3]
    xnat_passwd = sys.argv[4]
except:
    print("No exam ID specified, exiting...")
    exit(1)

with xnat.connect(xnat_url, user=xnat_user, password=xnat_passwd) as session:
    exam_data = session.experiments[exam_id]
    print(exam_data.subject.label)
