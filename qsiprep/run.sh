#!/bin/bash

mkdir /tmp/t1 /tmp/dwi /tmp/fmap

## Move/convert to nifti staging folders
if [ -d "/t1/NIFTI" ]; then
    cp /t1/NIFTI/* /tmp/t1
else
    dcm2niix -z y -d 9 -o /tmp/t1 /t1/DICOM
fi

if [ -d "/dwi/NIFTI" ]; then
    cp /dwi/NIFTI/* /tmp/dwi
else
    dcm2niix -z y -d 9 -o /tmp/dwi /dwi/DICOM
fi

if [ -d "/fmap/NIFTI" ]; then
    cp -v /fmap/NIFTI/* /tmp/fmap
else
    if [ -d "/fmap/DICOM" ]; then
        dcm2niix -z y -d 9 -o /tmp/fmap /fmap/DICOM
    else
        echo "No fieldmap"
    fi
fi

## TODO:
## Check if session has fieldmaps available, use --use-syn-sdc if not

find /tmp/t1 -type f -name "*.nii.gz" -exec mv -v {} /bids/sub-01/anat/sub-01_T1w.nii.gz \;
find /tmp/t1 -type f -name "*.json" -exec mv -v {} /bids/sub-01/anat/sub-01_T1w.json \;

find /tmp/dwi -type f -name "*.nii.gz" -exec mv -v {} /bids/sub-01/dwi/sub-01_dwi.nii.gz \;
find /tmp/dwi -type f -name "*.json" -exec mv -v {} /bids/sub-01/dwi/sub-01_dwi.json \;
find /tmp/dwi -type f -name "*.bval" -exec mv -v {} /bids/sub-01/dwi/sub-01_dwi.bval \;
find /tmp/dwi -type f -name "*.bvec" -exec mv -v {} /bids/sub-01/dwi/sub-01_dwi.bvec \;

tree /bids

/usr/local/miniconda/bin/qsiprep /bids /output  participant \
    --use-syn-sdc \
    -w /work \
    --output-resolution 2 \
    --ignore sbref \
    --eddy-config /eddy_params.json \
    --n_cpus $n_cores \
    --notrack
