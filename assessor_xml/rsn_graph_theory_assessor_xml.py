import begin
import json
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker


def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def generate_assessor_xml(graph_theory_measures, metric, xnat_host, project_name, session_id, session_label):

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = '{}_{}'.format(session_id, metric)
    assessorLabel = '{}_{}'.format(session_label, metric)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    if metric == 'pearson':
        element_name = 'RSNGraphTheoryMeasuresPearson'
    elif metric == 'sparse':
        element_name = 'RSNGraphTheoryMeasuresSparse'
    else:
        raise SystemExit

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E(element_name, assessorTitleAttributesDict,
                E(ns('xnat', 'date'), date),
                E(ns('xnat', 'imageSession_ID'), session_id),
                E(ns('crania', 'efficiency'), 
                  E(ns('crania', 'dmn'), graph_theory_measures['DMN']['efficiency']),
                  E(ns('crania', 'auditory'), graph_theory_measures['Auditory']['efficiency']),
                  E(ns('crania', 'sensorimotor'), graph_theory_measures['Sensorymotor']['efficiency']),
                  E(ns('crania', 'executiveControl'), graph_theory_measures['ExecutiveControl']['efficiency']),
                  E(ns('crania', 'frontoparietalRight'), graph_theory_measures['FrontoparietalRight']['efficiency']),
                  E(ns('crania', 'frontoparietalLeft'), graph_theory_measures['FrontoparietalLeft']['efficiency']),
                  E(ns('crania', 'V1'), graph_theory_measures['V1']['efficiency']),
                  E(ns('crania', 'V2'), graph_theory_measures['V2']['efficiency']),
                  E(ns('crania', 'V3'), graph_theory_measures['V3']['efficiency'])                  
                 ),
                E(ns('crania', 'assortativity'),
                  E(ns('crania', 'dmn'), graph_theory_measures['DMN']['assortativity']),
                  E(ns('crania', 'auditory'), graph_theory_measures['Auditory']['assortativity']),
                  E(ns('crania', 'sensorimotor'), graph_theory_measures['Sensorymotor']['assortativity']),
                  E(ns('crania', 'executiveControl'), graph_theory_measures['ExecutiveControl']['assortativity']),
                  E(ns('crania', 'frontoparietalRight'), graph_theory_measures['FrontoparietalRight']['assortativity']),
                  E(ns('crania', 'frontoparietalLeft'), graph_theory_measures['FrontoparietalLeft']['assortativity']),
                  E(ns('crania', 'V1'), graph_theory_measures['V1']['assortativity']),
                  E(ns('crania', 'V2'), graph_theory_measures['V2']['assortativity']),
                  E(ns('crania', 'V3'), graph_theory_measures['V3']['assortativity'])
                 ),
                E(ns('crania', 'transitivity'), 
                  E(ns('crania', 'dmn'), graph_theory_measures['DMN']['transitivity']),
                  E(ns('crania', 'auditory'), graph_theory_measures['Auditory']['transitivity']),
                  E(ns('crania', 'sensorimotor'), graph_theory_measures['Sensorymotor']['transitivity']),
                  E(ns('crania', 'executiveControl'), graph_theory_measures['ExecutiveControl']['transitivity']),
                  E(ns('crania', 'frontoparietalRight'), graph_theory_measures['FrontoparietalRight']['transitivity']),
                  E(ns('crania', 'frontoparietalLeft'), graph_theory_measures['FrontoparietalLeft']['transitivity']),
                  E(ns('crania', 'V1'), graph_theory_measures['V1']['transitivity']),
                  E(ns('crania', 'V2'), graph_theory_measures['V2']['transitivity']),
                  E(ns('crania', 'V3'), graph_theory_measures['V3']['transitivity'])
                )
            )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    with open(f'/output/{metric}_rsn_graph_theory_measures.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(metric: 'sparse',
        xnat_host,
        project_name,
        session_id,
        session_label):

    if metric == 'pearson':
        with open('/input/PEARSON_BCT/correlation_graph_theory_measures.json', 'r') as pearson_file:
            graph_theory_measures = json.load(pearson_file)
    elif metric == 'sparse':
        with open('/input/SPARSE_INVERSE_COVARIANCE_BCT/sparse_graph_theory_measures.json', 'r') as sparse_file:
            graph_theory_measures = json.load(sparse_file)
    else:
        print("Not a valid functional connectome metric")
        raise SystemExit

    for rsn, measures in graph_theory_measures.items():
        for measure in measures:
            graph_theory_measures[rsn][measure] = str(graph_theory_measures[rsn][measure])

    generate_assessor_xml(graph_theory_measures, metric, 
                          xnat_host, project_name, 
                          session_id, session_label)
