import begin
import pandas as pd
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker


def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def generate_assessor_xml(lqt, xnat_host, project_name, session_id, session_label, scan_id):

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = '{}_LQT'.format(session_id, scan_id)
    assessorLabel = '{}_LQT'.format(session_label, scan_id)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E('ThalamicStructuralDisconnectivity', assessorTitleAttributesDict,
        E(ns('xnat', 'date'), date),
        E(ns('xnat', 'imageSession_ID'), session_id),
        E(ns('crania', 'thalamicStructuralDisconnectivity'),str(lqt))
    )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    with open('/output/thalamicStructuralDisconnectivity.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(xnat_host,
        project_name,
        session_id,
        session_label,
        scan_id):

    lqt_stats = pd.read_csv("/input/LQT/lqt_stats.csv")
    r_thal = lqt_stats[ lqt_stats['variable'] == 'lqt_rdc_Right_Thalamus_Proper' ]
    r_thal_discon = r_thal['value'].values[0]
    l_thal = lqt_stats[ lqt_stats['variable'] == 'lqt_rdc_Left_Thalamus_Proper' ]
    l_thal_discon = l_thal['value'].values[0]
    mean_thal_discon = ( (r_thal_discon + l_thal_discon) / 2 ) / 100

    # shutil.copyfile('/inputlqt/stats/results.csv', '/output/SalientCentralLesionVolume.csv')

    generate_assessor_xml(mean_thal_discon, xnat_host, project_name, session_id, session_label, scan_id)
