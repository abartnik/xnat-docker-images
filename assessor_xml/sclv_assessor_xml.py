import begin
import shutil
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker


def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def generate_assessor_xml(sclv, xnat_host, project_name, session_id, session_label, scan_id):

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = '{}_SCLV'.format(session_id, scan_id)
    assessorLabel = '{}_SCLV'.format(session_label, scan_id)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E('SalientCentralLesionVolume', assessorTitleAttributesDict,
        E(ns('xnat', 'date'), date),
        E(ns('xnat', 'imageSession_ID'), session_id),
        E(ns('crania', 'salientCentralLesionVolume'), sclv)
    )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    with open('/output/salientCentralLesionVolume.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(xnat_host,
        project_name,
        session_id,
        session_label,
        scan_id):

    with open('/input/SCLV/sclv.csv', 'r') as infile:
        for line in infile.readlines():
            sclv = line.strip()

    # shutil.copyfile('/input/sclv/stats/results.csv', '/output/SalientCentralLesionVolume.csv')

    generate_assessor_xml(sclv, xnat_host, project_name, session_id, session_label, scan_id)
