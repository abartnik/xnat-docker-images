import begin
import json
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker


def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def generate_assessor_xml(mov, xnat_host, project_name, session_id, session_label, scan_id):

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = '{}_MOV'.format(session_id, scan_id)
    assessorLabel = '{}_MOV'.format(session_label, scan_id)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E('MedullaOblongataVolume', assessorTitleAttributesDict,
        E(ns('xnat', 'date'), date),
        E(ns('xnat', 'imageSession_ID'), session_id),
        E(ns('crania', 'medullaOblongataVolume'), mov)
    )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    with open('/output/medullaOblongataVolume.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(xnat_host,
        project_name,
        session_id,
        session_label,
        scan_id):

    with open('/input/MOV/output.json', 'r') as infile:
        results = json.load(infile)
    mov = results['adjusted_medulla_volume']

    # shutil.copyfile('/input/mov/stats/results.csv', '/output/SalientCentralLesionVolume.csv')

    generate_assessor_xml(mov, xnat_host, project_name, session_id, session_label, scan_id)
