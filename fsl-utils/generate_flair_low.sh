#!/bin/bash

flirt -v \
    -in /flair/flair_std_n4.nii.gz \
    -ref /proxy/proxy_std_n4.nii.gz \
    -dof 6 \
    -omat /tmp/flair2proxy.mat \
    -out /output/flair2proxy.nii.gz

convert_xfm \
    -omat /output/flair.mat \
    -concat /proxy/proxy_full2std_rigid.mat \
    /tmp/flair2proxy.mat

applywarp -v \
    --rel --interp=spline \
    -i /flair/flair_std_n4.nii.gz \
    -r /opt/fsl-6.0.4/data/standard/MNI152_1x1x3_refspace.nii.gz \
    --premat=/output/flair.mat \
    -o /output/flair_low.nii.gz

# flirt \
#     -in /flair/flair_std_n4.nii.gz \
#     -ref /opt/fsl-6.0.4/data/standard/MNI152_1x1x3_refspace.nii.gz \
#     -applyxfm -init /output/flair2mni.mat \
#     -out /output/flair_low.nii.gz

# slicer /output/flair_low.nii.gz /opt/fsl-6.0.4/data/standard/MNI152_1x1x3_refspace.nii.gz -e 0.5 -a /output/qcimg.png
