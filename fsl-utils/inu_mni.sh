#!/bin/bash

input=$1
nifti=$2

# dcm2niix -z y -d 9 -o /tmp/ -f flair $input 
if [[ "$nifti" == "nifti" ]]; then
    find "/input/NIFTI/" -type f -name "*nii.gz" -exec cp -v {} /tmp/flair.nii.gz \;
else
    find "$input" -type f -name "*nii.gz" -exec cp -v {} /tmp/flair.nii.gz \;
fi

fslreorient2std /tmp/flair.nii.gz /tmp/flair_std.nii.gz

N4BiasFieldCorrection \
    --image-dimensionality 3 \
    --input-image /tmp/flair_std.nii.gz \
    --output /output/flair_std_n4.nii.gz \
    --shrink-factor 4 \
    --convergence [50x50x50x50,0.0000001] \
    --bspline-fitting [200]

# flirt \
#     -in /tmp/n4_flair.nii.gz  \
#     -ref /opt/fsl-6.0.4/data/standard/MNI152_T1_2mm.nii.gz \
#     -omat /output/flair.mat \
#     -out /output/flair_low.nii.gz
