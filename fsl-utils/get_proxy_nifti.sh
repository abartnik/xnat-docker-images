#!/bin/bash

url=$1
username=$2
password=$3
session_id=$4
nifti=$5

echo $url $username $password $session_id
series_number=$(/opt/fsl-6.0.4/fslpython/bin/python3 /usr/bin/find_proxy.py $url $username $password $session_id)
echo "$series_number"

if [[ "$nifti" == "nifti" ]]; then
    find "/input/SCANS/$series_number/NIFTI/" -type f -name "*nii.gz" -exec cp -v {} /proxy/proxy.nii.gz \;
else
    dcm2niix -z y -f "proxy" -o /proxy "/input/SCANS/$series_number"
fi

fslreorient2std /proxy/proxy.nii.gz /tmp/proxy_std.nii.gz

N4BiasFieldCorrection \
    --image-dimensionality 3 \
    --input-image /tmp/proxy_std.nii.gz \
    --output /proxy/proxy_std_n4.nii.gz \
    --shrink-factor 4 \
    --convergence [50x50x50x50,0.0000001] \
    --bspline-fitting [200]

robustfov -i /proxy/proxy_std_n4.nii.gz -m /tmp/proxy_roi2full.mat -r /tmp/proxy_robustroi.nii.gz 

convert_xfm -omat /tmp/proxy_full2roi.mat -inverse /tmp/proxy_roi2full.mat 

flirt \
    -in /tmp/proxy_robustroi.nii.gz \
    -ref /opt/fsl-6.0.4/data/standard/MNI152_T1_1mm.nii.gz \
    -omat /tmp/proxy_roi2std.mat \
    -searchrx -30 30 -searchry -30 30 -searchrz -30 30  \
    -out /proxy/proxy_reg.nii.gz

convert_xfm -omat /tmp/proxy_full2std.mat -concat /tmp/proxy_roi2std.mat /tmp/proxy_full2roi.mat 

aff2rigid /tmp/proxy_full2std.mat /proxy/proxy_full2std_rigid.mat

slicer /proxy/proxy_reg.nii.gz /opt/fsl-6.0.4/data/standard/MNI152_T1_1mm.nii.gz -e 0.5 -a /proxy/qcimg.png
