#!/bin/bash

export PATH=$PATH:/usr/local/c3d/bin
# export ANTSPATH=/usr/lib/ants/

## Warp proxy to MNI 1x1x1
antsRegistrationSyN.sh -d 3 \
    -f $FSLDIR/data/standard/MNI152_T1_1mm.nii.gz \
    -m /proxy/proxy_std_n4.nii.gz \
    -o /tmp/proxy_to_mni \
    -n 10 \
    -t s

## Get rigid MNI to proxy transform
convert_xfm -omat \
    /tmp/proxy_full2std_rigid_inv.mat \
    -inverse /proxy/proxy_full2std_rigid.mat

c3d_affine_tool /tmp/proxy_full2std_rigid_inv.mat \
    -ref /proxy/proxy_std_n4.nii.gz \
    -src /flair/flair_low.nii.gz \
    -fsl2ras \
    -oitk /tmp/rigidMNI2proxy_ants.mat

## Apply transforms
antsApplyTransforms -d 3 \
    -i /flair/flair_low.nii.gz \
    -r $FSLDIR/data/standard/MNI152_T1_1mm.nii.gz \
    -t /tmp/proxy_to_mni1Warp.nii.gz \
    -t /tmp/proxy_to_mni0GenericAffine.mat \
    -t /tmp/rigidMNI2proxy_ants.mat \
    -o /output/flair_high.nii.gz

slicer /output/flair_high.nii.gz /opt/fsl-6.0.4/data/standard/MNI152_T1_1mm.nii.gz -e 0.5 -a /output/qcimg.png
