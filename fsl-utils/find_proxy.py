import xnat
import os
import begin


@begin.start
def main(url: "The URL of the XNAT server",
         username: "Username in XNAT",
         password: "Password in XNAT",
         session_id: "Exam/session ID of MRI exam from XNAT"):

    proxy_series_number = 0
    with xnat.connect(url, user=username, password=password) as session:
        exam = session.experiments[session_id]
        
        scan_types = { scan.type: series_number for series_number, scan in exam.scans.items() }

        if 'MPRAGE' in scan_types.keys():
            proxy_series_number = scan_types['MPRAGE']
        elif 'SPGR' in scan_types.keys():
            proxy_series_number = scan_types['SPGR']
        elif 'T1w' in scan_types.keys():
            proxy_series_number = scan_types['T1w']
        elif 'FLAIR' in scan_types.keys():
            proxy_series_number = scan_types['FLAIR']
        else:
            exit(1)
    
    print(proxy_series_number)
