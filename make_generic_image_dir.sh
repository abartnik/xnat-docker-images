#!/bin/bash

image_name=$1

mkdir "$image_name"
touch "$image_name/Dockerfile"
touch "$image_name/run.sh"
cp debug-command.json "$image_name/command.json"
