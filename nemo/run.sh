#!/bin/bash

if [ ! -d /flair ] ;
then
    echo "/flair not mounted"
    exit 1
fi
if [ ! -d /fll ] ;
then
    echo "/fll not mounted"
    exit 1
fi

find /flair -type f -name '*nii.gz' -exec cp -v {} /workdir/flair.nii.gz \;
find /fll -type f -name '*nii.gz' -exec cp -v {} /workdir/fll.nii.gz \;
cp /nemo-runner/Makefile /workdir

cd /workdir && make 

cp /workdir/ChaCo86_MNI.mat /output
