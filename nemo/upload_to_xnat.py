import xnat
from xnat.exceptions import XNATResponseError
import begin
import numpy as np
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker


def create_nemo_resource(url, username, password, project, exam_id, scan_id) -> None:
    with xnat.connect(url, user=username, password=password) as xnat_session:
        try:
            scan_data = xnat_session.projects[project].experiments[exam_id].scans[str(scan_id)]
        except XNATResponseError:
            print("Something weird is going on with the server, uploading nemo anyway")
            pass
        try:
            scan_data.create_resource('CHACO')
        except:
            print(f"Error creating resource for {exam_id}, probably already exists on server...")

def upload_nemo(url, username, password, project, subject_id, exam_id, scan_id, discon_mat, nemo_sc) -> None:
    with xnat.connect(url, user=username, password=password) as xnat_session:
        discon_remote_path = url + "/data/projects/" + project + "/subjects/" + subject_id + \
            "/experiments/" + exam_id + "/scans/" + str(scan_id) + "/resources/CHACO/files/" + discon_mat.split("/")[-1]
        sc_remote_path = url + "/data/projects/" + project + "/subjects/" + subject_id + \
            "/experiments/" + exam_id + "/scans/" + str(scan_id) + "/resources/CHACO/files/" + nemo_sc.split("/")[-1]
        
        xnat_session.upload(discon_remote_path, discon_mat)
        xnat_session.upload(sc_remote_path, discon_mat)
        print(f"Uploaded nemo for {exam_id}")

def generate_assessor_xml(data, datatype, xnat_host, project_name, session_id, session_label):
    nsdict = {
        'xnat': 'http://nrg.wustl.edu/xnat',
        'xs': 'http://www.w3.org/2001/XMLSchema-instance',
        'crania': 'http://xnat.cbi.bnac.net/crania',
        'cdd': 'http://xnat.cbi.bnac.net/cdd'
    }

    def ns(namespace, tag):
        return "{%s}%s"%(nsdict[namespace], tag)

    def schemaLoc(namespace, xnat_host):
        return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = f'{session_id}_{datatype}'
    assessorLabel = f'{session_label}_{datatype}'
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 
                                                                                              'crania', 
                                                                                              'cdd'))
    }
    xml_element_names = {
        'NEMOTV': {
            "element_name": "ThalamicStructuralDisconnectivity",
            "element": "thalamicStructuralDisconnectivity"
        },
        'NEMONE': {
            "element_name": "NeMoNetworkEfficiency",
            "element": "nemoNetworkEfficiency"
        }
    }
    
    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E(xml_element_names[datatype]['element_name'], assessorTitleAttributesDict,
        E(ns('xnat', 'date'), date),
        E(ns('xnat', 'imageSession_ID'), session_id),
        E(ns('crania', xml_element_names[datatype]['element']), str(data))
    )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    return(xml_contents)

def upload_thal_discon_assessor(url, username, password, project, exam_id, mean_thal_discon):
    with xnat.connect(url, user=username, password=password) as xnat_session:
        session_data = xnat_session.projects[project].experiments[exam_id]
        session_id = session_data.id
        session_label = session_data.label

        xml_contents = generate_assessor_xml(mean_thal_discon, 'NEMOTV', 
                                                url, project,
                                                session_id, session_label)
        xnat_session.upload(f'/data/experiments/{session_id}/assessors', 
                                xml_contents,
                                content_type="application/xml",
                                method="post",
                                overwrite=True)

def upload_efficiency_assessor(url, username, password, project, exam_id, nemo_sc):
    with xnat.connect(url, user=username, password=password) as xnat_session:
        session_data = xnat_session.projects[project].experiments[exam_id]
        session_id = session_data.id
        session_label = session_data.label

        xml_contents = generate_assessor_xml(nemo_sc, 'NEMOGE', 
                                                url, project,
                                                session_id, session_label)
        xnat_session.upload(f'/data/experiments/{session_id}/assessors', 
                                xml_contents,
                                content_type="application/xml",
                                method="post",
                                overwrite=True)

@begin.start
def main(xnat_host: "XNAT URL",
         xnat_username: "XNAT username",
         xnat_password: "XNAT password",
         exam_id: "ID of exam to upload data for",
         scan_id: "ID of FLAIR scan to map NeMo to"):
    
    discon_mat = np.genfromtxt("/output/chacoconn_fs86subj_mean.csv", delimiter=",")
    r_thal_pct = np.mean(discon_mat[69])
    l_thal_pct = np.mean(discon_mat[78])
    mean_thal_discon = ( (l_thal_pct + r_thal_pct) / 2)

    nemo_sc = np.genfromtxt("/output/nemoSC.txt")

    with xnat.connect(xnat_host, user=xnat_username, password=xnat_password) as session:
        exam_data = session.experiments[exam_id]
        project = exam_data.project
        subject_data = session.subjects[exam_data.subject_id]
        subject_id = subject_data.label
    
    create_nemo_resource(xnat_host, xnat_username, xnat_password, 
                         project, exam_id, scan_id)
    upload_nemo(xnat_host, xnat_username, xnat_password, 
                project, subject_id, exam_id, scan_id, 
                "/output/chacoconn_fs86subj_mean.csv", "/output/nemoSC.txt")
    
    upload_thal_discon_assessor(xnat_host, xnat_username, xnat_password, project, exam_id, mean_thal_discon)
    # upload_efficiency_assessor(xnat_host, xnat_username, xnat_password, project, exam_id, nemo_sc)
