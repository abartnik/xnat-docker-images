import pickle
import numpy as np

data = pickle.load(open('/output/_chacoconn_fs86subj_mean.pkl', 'rb'))
m = np.triu(data.toarray()) + np.triu(data.toarray(), -1).T
np.savetxt('/output/chacoconn_fs86subj_mean.csv', m, delimiter=',')
