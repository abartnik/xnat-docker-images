#!/bin/bash

cp -v /shared/home/abartnik/brain/raw_flair.nii flair.nii.gz
cp -v /shared/home/abartnik/brain/nemo/fll.nii.gz fll.nii.gz

curl http://localhost:5000/nemo -v \
  -F "image=registry.bnac.net/crania/nemo:0.3" \
  -F "command=run-submitted-job.sh" \
  -F "inputs[]=@flair.nii.gz" \
  -F "inputs[]=@fll.nii.gz" \
  -X PUT \
  --output test.tar.gz

tar zxvf test.tar.gz -C test_results

