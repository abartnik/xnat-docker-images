#!/bin/bash

cp -v /input/flair.nii.gz /workdir
cp -v /input/fll.nii.gz /workdir
cp -v /nemo-runner/Makefile /workdir

echo "Starting processing..."
cd /workdir && make 

cp -v /workdir/ChaCo86_MNI.mat /output