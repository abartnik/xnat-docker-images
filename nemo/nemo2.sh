#!/bin/bash

xnat_host=$1
xnat_username=$2
xnat_password=$3
exam_id=$4
scan_id=$5

python3 /opt/nemo2/nemo_lesion_to_chaco.py \
    --lesion /FLL_HIGH/fll_high.nii.gz \
    --outputbase /output/ \
    --chunklist /data/nemo_chunklist.npz \
    --chunkdir /data/chunkfiles \
    --refvol /opt/nemo2/website/atlases/MNI152_T1_1mm_brain.nii.gz \
    --endpoints /data/nemo_endpoints.npy \
    --asum /data/nemo_Asum_endpoints.npz \
    --asum_weighted /data/nemo_Asum_weighted_endpoints.npz \
    --asum_cumulative /data/nemo_Asum_cumulative.npz \
    --asum_weighted_cumulative /data/nemo_Asum_weighted_cumulative.npz \
    --trackweights /data/nemo_siftweights.npy \
    --tracklengths /data/nemo_tracklengths.npy \
    --tracking_algorithm ifod2act \
    --pairwise \
    --parcelvol /data/nemo_atlases/fs86_dil1_allsubj.npz=fs86subj \
    --ncores 4

python3 /usr/local/bin/pickle_to_csv.py

python3 /opt/nemo2/chacoconn_to_nemosc.py \
    --chacoconn /output/_chacoconn_fs86subj_allref.pkl \
    --denom /output/_chacoconn_fs86subj_allref_denom.pkl \
    --output /output/nemoSC.txt

python3 /usr/local/bin/upload_to_xnat.py $xnat_host $xnat_username $xnat_password $exam_id $scan_id
