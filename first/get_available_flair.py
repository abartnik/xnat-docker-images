#!/opt/fsl-6.0.4/fslpython/bin/python3

import begin
import xnat
from time import sleep


def check_flair_available(url, username, password, session_id):

    def check_flair():
        return([scan_data for scan_id, scan_data \
                in exam.scans.items() if "FLAIR" in scan_data.type])

    def fll_ready():
        return([ resource_data for resource_id, resource_data in scan_data.resources.items() \
                if "FLL_BIN" in resource_data.label ])

    with xnat.connect(url, user=username, password=password) as session:
        exam = session.experiments[session_id]
        
        possible_flair = check_flair()
        if len(possible_flair) > 0:
            scan_data = possible_flair[0]
            # scan_data.download_dir("/tmp/flair")
            while len(fll_ready()) == 0:
                sleep(10)
            fll_bin = fll_ready()[0]
            print(fll_bin.download_dir("/tmp/fll"))

            for resource_id, resource_data in scan_data.resources.items():
                if "FLAIR_LOW" in resource_data.label:
                    resource_data.download_dir("/tmp/flair")
        
        else:
            print("No FLAIR found")

@begin.start
def main(url: "The URL of the XNAT server",
         username: "Username in XNAT",
         password: "Password in XNAT",
         session_id: "Exam/session ID of MRI exam from XNAT"):

        flair = check_flair_available(url, username, password, session_id)
        if flair is not None:
            print(flair)
        else:
            print("No flair")
