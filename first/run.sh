#!/bin/bash

t1=$1
# flair=$2
# fll=$3

url=$2
username=$3
password=$4
session_id=$5

find "$t1" -type f -name "*nii" -exec gzip {} \;
find "$t1" -type f -name "*nii.gz" -exec cp -v {} /work/t1.nii.gz \;

get_available_flair.py $url $username $password $session_id
find /tmp/flair -type f -name "flair_low.nii.gz" -exec cp -v {} /tmp/flair/flair.nii.gz \;
find /tmp/fll -type f -name "*nii.gz" -exec cp -v {} /tmp/fll/fll.nii.gz \;

if [ -f /tmp/flair/flair.nii.gz ] ; then 
    t1_lesion_inpainting.sh /work/t1.nii.gz /tmp/flair/flair.nii.gz /tmp/fll/fll.nii.gz
    cp -v /work/t1_inpainted.nii.gz /work/t1.nii.gz
fi

run.py
# /bin/calc_stats.py /output/first_all_fast_firstseg.nii.gz > /output/stats.csv
