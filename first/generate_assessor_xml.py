#!/opt/fsl-6.0.4/fslpython/bin/python3

import begin
import xnat
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker
from pathlib import Path
import subprocess
from shutil import copy
from time import sleep


def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def get_normalization_factor(xnat_url, exam_id, scan_id, username, password):

    def sienax_ready(exam_id, scan_id):
        with xnat.connect(xnat_url, user=username, password=password) as session:
            scan_data = session.experiments[exam_id].scans[scan_id]
        
            return([ resource_id for resource_id, resource_data in scan_data.resources.items() \
                if "SIENAX" in resource_data.label ])

    with xnat.connect(xnat_url, user=username, password=password) as session:
        exam_data = session.experiments[exam_id]

        while len(sienax_ready(exam_id, scan_id)) == 0:
            sleep(60)

        for assessor_id, assessor_data in exam_data.assessors.items():
            if "SIENAX" in assessor_id:
                normalization_factor = assessor_data.whole_brain_volume.normalized_whole_brain_volume / \
                    assessor_data.whole_brain_volume.nonnormalized_whole_brain_volume
        return(normalization_factor)

def generate_assessor_xml(first_results, xnat_host, project_name, session_id, session_label, scan_id, username, password):

    normalization_factor = get_normalization_factor(xnat_host, session_label, scan_id, username, password)

    now = datetime.today()
    timestamp = now.strftime('%Y%m%d%H%M%S%f')
    assessorId = '{}_FIRST'.format(session_id)
    assessorLabel = '{}_FIRST'.format(session_label)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E('First', assessorTitleAttributesDict,
                    E(ns('xnat', 'date'), date),
                    E(ns('xnat', 'imageSession_ID'), session_id),

                    E(ns('crania', 'leftThalamusVolume'), 
                    E(ns('crania', 'normalizedLeftThalamusVolume'), str(first_results['first-thal-l'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedLeftThalamusVolume'), str(first_results['first-thal-l']))
                    ),

                    E(ns('crania', 'leftCaudateVolume'),
                    E(ns('crania', 'normalizedLeftCaudateVolume'), str(first_results['first-caud-l'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedLeftCaudateVolume'), str(first_results['first-caud-l']))
                    ),

                    E(ns('crania', 'leftPutamenVolume'), 
                    E(ns('crania', 'normalizedLeftPutamenVolume'), str(first_results['first-puta-l'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedLeftPutamenVolume'), str(first_results['first-puta-l']))
                    ),

                    E(ns('crania', 'leftPallidumVolume'), 
                    E(ns('crania', 'normalizedLeftPallidumVolume'), str(first_results['first-pall-l'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedLeftPallidumVolume'), str(first_results['first-pall-l']))
                    ),

                    E(ns('crania', 'leftHippocampusVolume'), 
                    E(ns('crania', 'normalizedLeftHippocampusVolume'), str(first_results['first-hipp-l'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedLeftHippocampusVolume'), str(first_results['first-hipp-l']))
                    ),

                    E(ns('crania', 'leftAmygdalaVolume'), 
                    E(ns('crania', 'normalizedLeftAmygdalaVolume'), str(first_results['first-amyg-l'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedLeftAmygdalaVolume'), str(first_results['first-amyg-l']))
                    ),

                    E(ns('crania', 'leftNucleusAccumbens'), 
                    E(ns('crania', 'normalizedLeftNucleusAccumbens'), str(first_results['first-nacc-l'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedLeftNucleusAccumbens'), str(first_results['first-nacc-l']))
                    ),

                    E(ns('crania', 'rightThalamusVolume'), 
                    E(ns('crania', 'normalizedRightThalamusVolume'), str(first_results['first-thal-r'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedRightThalamusVolume'), str(first_results['first-thal-r']))
                    ),

                    E(ns('crania', 'rightCaudateVolume'), 
                    E(ns('crania', 'normalizedRightCaudateVolume'), str(first_results['first-caud-r'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedRightCaudateVolume'), str(first_results['first-caud-r']))
                    ),

                    E(ns('crania', 'rightPutamenVolume'), 
                    E(ns('crania', 'normalizedRightPutamenVolume'), str(first_results['first-puta-r'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedRightPutamenVolume'), str(first_results['first-puta-r']))
                    ),

                    E(ns('crania', 'rightPallidumVolume'), 
                    E(ns('crania', 'normalizedRightPallidumVolume'), str(first_results['first-pall-r'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedRightPallidumVolume'), str(first_results['first-pall-r']))
                    ),

                    E(ns('crania', 'rightHippocampusVolume'), 
                    E(ns('crania', 'normalizedRightHippocampusVolume'), str(first_results['first-hipp-r'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedRightHippocampusVolume'), str(first_results['first-hipp-r']))
                    ),

                    E(ns('crania', 'rightAmygdalaVolume'), 
                    E(ns('crania', 'normalizedRightAmygdalaVolume'), str(first_results['first-amyg-r'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedRightAmygdalaVolume'), str(first_results['first-amyg-r']))
                    ),
                    
                    E(ns('crania', 'rightNucleusAccumbensVolume'), 
                    E(ns('crania', 'normalizedRightNucleusAccumbensVolume'), str(first_results['first-nacc-r'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedRightNucleusAccumbensVolume'), str(first_results['first-nacc-r']))
                    ),

                    E(ns('crania', 'brainstemVolume'), 
                    E(ns('crania', 'normalizedBrainstemVolume'), str(first_results['first-brainstem'] * normalization_factor)),
                    E(ns('crania', 'nonnormalizedBrainstemVolume'), str(first_results['first-brainstem']))
                    )
                )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    # print(xml_contents)
    with open('/output/first.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(xnat_host,
        project_name,
        session_id,
        session_label,
        scan_id,
        username,
        password):

    if not Path("/input/FIRST/stats.csv").exists():
        bashCommand = "/bin/calc_stats.py /input/FIRST/first_all_fast_firstseg.nii.gz /tmp/stats.csv"
        subprocess.call(bashCommand.split())
    else:
        copy("/input/FIRST/stats.csv", "/tmp/stats.csv")

    first_results = {}
    with open("/tmp/stats.csv", "r") as stats_file:
        for line in stats_file.readlines():
            roi, vol = line.strip().split(",")
            first_results[roi] = float(vol)
    print(first_results)

    generate_assessor_xml(first_results, xnat_host, project_name, session_id, 
                          session_label, scan_id, username, password)
