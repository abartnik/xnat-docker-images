#!/opt/fsl-6.0.4/fslpython/bin/python3

import nibabel
import optparse
from numpy import prod
from numpy import sum


usage = """%prog [options] first-seg-map"

Warning: this program is BNAC VRMQC, and should be used with appropriate
caution. It is not designed for use without manual review of the
outputs."""
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
        help="Increase verbosity", action="store_true", default=False)
(options, args) = parser.parse_args()

segmap =nibabel.load(args[0])

segmapdata = segmap.get_fdata()

zooms = segmap.header.get_zooms()
voxvol = prod(zooms)

regions = [
	(10, 'first-thal-l'),
	(11, 'first-caud-l'),
	(12, 'first-puta-l'),
	(13, 'first-pall-l'),
	(17, 'first-hipp-l'),
	(18, 'first-amyg-l'),
	(26, 'first-nacc-l'),
	(49, 'first-thal-r'),
	(50, 'first-caud-r'),
	(51, 'first-puta-r'),
	(52, 'first-pall-r'),
	(53, 'first-hipp-r'),
	(54, 'first-amyg-r'),
	(58, 'first-nacc-r'),
	(16, 'first-brainstem')
]

with open(args[1], "w") as outfile:
    for region_code, region_name in regions:
        region_vol = float(sum(segmapdata == region_code)) * voxvol
        outfile.write(f"{region_name},{region_vol}\n")
