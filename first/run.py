#!/opt/fsl-6.0.4/fslpython/bin/python3

import os
import json
import subprocess
import glob


# Opening JSON file
f = open('/work/config.json')
config = json.load(f)
f.close()

# This is the main function to be implemented. It needs to work from the config data, passed in as input.
# It should always output a file called output.json

def run_analysis(input):
	input["first_opts"] = input["first_opts"] if "first_opts" in input else ""
	bashCommand = "run_first_all -i {infile} -o /output/first {first_opts}".format(**input)
	outlog = open('/output/log.txt', 'w')
	errlog = open('/output/error.txt', 'w')
	returncode = subprocess.call(bashCommand.split(), stdout=outlog, stderr=errlog)
	outlog.close()
	errlog.close()
	if returncode == 0:
		output = {"first_opts": input["first_opts"],
				"qcimg": "qcimg.png"
				}
		make_montage()
		calc_stats()
		with open("/output/output.json", "w") as outfile:
			json.dump(output, outfile)


def calc_stats():
	bashCommand = "/bin/calc_stats.py /output/first_all_fast_firstseg.nii.gz /output/stats.csv"
	subprocess.call(bashCommand.split())


def make_montage():
	bashCommand = "first_roi_slicesdir {infile} /output/first_all_fast_firstseg.nii.gz".format(**config)
	subprocess.call(bashCommand.split())
	qcfile = glob.glob("slicesdir/*firstseg*.png")[0]
	bashCommand = "mv {qcfile} /output/qcimg.png".format(qcfile=qcfile)
	subprocess.call(bashCommand.split())


run_analysis(config)
