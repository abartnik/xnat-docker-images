#!/bin/bash

find /input -type f -name "*nii.gz" -exec cp -v {} /tmp/flair.nii.gz \;
fslmaths /tmp/flair.nii.gz -s 2 /output/smoothed.nii.gz
