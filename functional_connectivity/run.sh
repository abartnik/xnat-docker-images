#!/bin/bash

sub_id=$1
measure=$2

python3 /usr/local/bin/generate_gm_mask.py /fmriprep/sub-$sub_id/func/sub-"$sub_id"_task-rest_space-MNI152NLin2009cAsym_desc-aparcaseg_dseg.nii.gz

fsl_regfilt \
    -i /fmriprep/sub-$sub_id/func/sub-"$sub_id"_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz \
    -o /tmp/Denoised_data.nii.gz \
    -d /fmriprep/sub-01/func/sub-"$sub_id"_task-rest_desc-MELODIC_mixing.tsv \
    -f $(cat /fmriprep/sub-"$sub_id"/func/sub-"$sub_id"_task-rest_AROMAnoiseICs.csv)

python3 /usr/local/bin/FunctionalConnectome.py /tmp/Denoised_data.nii.gz /tmp/GMmask.nii.gz /fmriprep/sub-"$sub_id"/func/sub-"$sub_id"_task-rest_desc-confounds_timeseries.tsv $measure

python3 /usr/local/bin/rsn_connectome.py $measure
