import begin
import pandas as pd

import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
    warnings.filterwarnings("ignore", category=RuntimeWarning)
    import numpy as np
    try:
        from sklearn.covariance import GraphicalLassoCV
    except ImportError:
        # for Scitkit-Learn < v0.20.0
        from sklearn.covariance import GraphLassoCV as GraphicalLassoCV

import nibabel
import nilearn.regions
from nilearn.input_data import NiftiLabelsMasker
from nilearn.connectome import ConnectivityMeasure


def create_functional_connectivity_matrix(denoised_fmri, gm_mask, all_confounds, measure):
    """ Performs functional connectivity analysis on a subject
        using temporal correlation and sparse inverse covariance
        of activity between brain regions across time
    """

    all_confounds = pd.read_csv(all_confounds, sep='\t')
    interested_confounds = all_confounds[["global_signal", "csf", "white_matter", 
                                          "trans_x", "trans_y", "trans_z", 
                                          "rot_x", "rot_y", "rot_z"]]
    interested_confounds.to_csv('/tmp/confounds.csv', header=False, index=False)

    # Focus on signal only coming from voxels in grey matter
    regions = nibabel.load(gm_mask)
    fmri4D = nibabel.load(denoised_fmri)
    nilearn.regions.img_to_signals_labels(fmri4D, regions)
    print("Extracted fMRI signal")
    masker = NiftiLabelsMasker(labels_img=regions, standardize=True,
                                    memory='nilearn_cache', verbose=5)
    time_series = masker.fit_transform(denoised_fmri,
                                            confounds='/tmp/confounds.csv')

    # Masks out the CSF signal, WM signal, and motion confounds
    if measure == "correlation":
        # Obtain functional connectivity matrix using full correlation
        try:
            correlation_measure = ConnectivityMeasure(kind='correlation')
            correlation_matrix = correlation_measure.fit_transform([time_series])[0]
            # np.fill_diagonal(correlation_matrix, 0)
            with open('/output/functional_connectome.csv', 'wb') as outfile:
                np.savetxt(outfile, correlation_matrix, delimiter=",")
        except:
            print("Couldn't do full correlation connectome!")
            with open('/output/functional_connectome.csv', 'wb') as outfile:
                np.savetxt(outfile, np.zeros([86,86]), delimiter=",")

    # Obtain the "partion correlation" matrix using sparse inverse covariance
    elif measure == "sparse":
        try:
            estimator = GraphicalLassoCV()
            estimator.fit(time_series)
            estimator.precision_
            sparse_inverse_covariance = np.ma.array(-estimator.precision_)
            # np.fill_diagonal(estimator.precision_, 0)
            with open('/output/functional_connectome_sparse_inverse.csv', 'wb') as outfile:
                np.savetxt(outfile, sparse_inverse_covariance, delimiter=",")
        except:
            print("Couldn't do sparse inverse covariance connectome!")
            with open('/output/functional_connectome_sparse_inverse.csv', 'wb') as outfile:
                np.savetxt(outfile, np.zeros([86,86]), delimiter=",")

@begin.start
def main(denoised_fmri, gm_mask, all_confounds, measure):
    """
    Takes a denoised 4D fMRI, a grey matter atlas, and a list of cofounds and
    computes functional connectomes using full Pearson correlation or 
    sparse inverse covariance
    """

    create_functional_connectivity_matrix(denoised_fmri, gm_mask, all_confounds, measure)
