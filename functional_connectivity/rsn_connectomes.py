import os
import csv
import numpy as np
import begin


@begin.start
def main(measure):
    """
    Creates a functional connectome for each resting state network
    as a subset of the entire connectome
    """

    if measure == "correlation":
        connectome = np.recfromtxt('/output/functional_connectome.csv', delimiter=',')
        measure_str = "_pearson"
    elif measure == "sparse":
        connectome = np.recfromtxt('/output/functional_connectome_sparse_inverse.csv', delimiter=',')
        measure_str = "_sparse_inverse_convariance"

    labels = np.recfromtxt('/atlas_labels.txt', dtype='str')
    labels_indexed = {}
    for label in labels:
        index = list(labels).index(label)
        labels_indexed[label] = index
    pairwise_labels = []
    for i, label_x in enumerate(labels):
        for j, label_y in enumerate(labels):
            pairwise_labels.append("_".join((label_x, label_y)))
    labels_array = np.asarray(pairwise_labels).reshape((86,86))

    upper_triangle = []
    for element in connectome[np.triu_indices(connectome.shape[1])]:
        upper_triangle.append(element)
    
    unique_labels = []
    diagonal_index = 0
    for i in upper_triangle:
        if i != 1.0:
            pair = labels_array[np.where(connectome == i)][0]
        else:
            pair = labels_array[np.where(connectome == i)][diagonal_index]
            diagonal_index += 1
        unique_labels.append(pair)
    
    long_fc_dict = {}
    for index, i in enumerate(unique_labels):
        long_fc_dict[unique_labels[index]] = upper_triangle[index]
    with open('/output/long_fc' + measure_str + '.csv', 'w') as long_file:  
        w = csv.DictWriter(long_file, long_fc_dict.keys())
        w.writeheader()
        w.writerow(long_fc_dict)
    
    def rsn_matrix(rsn_name, measure):
        rsn_labels = list(np.recfromtxt(rsn_name, dtype='str'))
        rsn_regions = [ labels_indexed[region] for region in rsn_labels ]
        rsn_rows = measure[rsn_regions]
        rsn_mat = rsn_rows[:,rsn_regions]
        return(rsn_mat)

    for rsn in os.listdir('/networks/'):
        rsn_name = rsn.split('/')[-1].split('.')[0].split('Smith')[-1]
        rsn_mat = rsn_matrix('/networks/' + rsn, connectome)
        with open('/output/' + rsn_name + measure_str + '.csv', 'wb') as outfile:
            np.savetxt(outfile, rsn_mat, delimiter=",")
