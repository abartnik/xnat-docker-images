import scipy.cluster.hierarchy as sch
import numpy as np
import matplotlib.pyplot as plt


figure(10,10)
full = np.recfromtxt('/output/functional_connectome.csv', delimiter=',')

Y = sch.linkage(full, method='centroid')
Z1 = sch.dendrogram(Y, orientation='right')

permutation_full = Z1['leaves']
figure()

plt.imshow(full[permutation_full][:,permutation_full], interpolation="nearest",
           vmax=1, vmin=-1, cmap=plt.cm.RdBu_r)
plt.colorbar()

labels = np.recfromtxt('/tmp/atlas_labels.txt', dtype='str')

labels_clustered = np.array(labels)[permutation_full]

x_ticks = plt.xticks(range(len(labels_clustered)), labels_clustered, rotation=90)
y_ticks = plt.yticks(range(len(labels_clustered)), labels_clustered)
plt.subplots_adjust(left=1, bottom=5, top=6, right=4)
plt.savefig('/output/functional_connectome.png', dpi='figure')
