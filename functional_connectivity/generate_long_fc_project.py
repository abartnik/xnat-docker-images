#!/usr/bin/env python

from glob import glob
import pandas as pd
import begin


@begin.start
def main(measure):
    if measure == "correlation":
        measure_str = "FUNCTIONAL_CONNECTOMES_CORRELATION"
    elif measure == "sparse":
        measure_str = "FUNCTIONAL_CONNECTOMES_SPARSE"

    path = "/input/**/SCANS/**/" + measure_str + "/long_fc*.csv"
    files = glob(path, recursive=True)
    subjects = [ filename.split('/')[2] for filename in files ]

    df = pd.concat( (pd.read_csv(filename) for filename in files) )
    df.insert(0, 'session_id', subjects)
    df.to_csv('/output/all_subjects_long_fc_' + measure + '.csv', index=False)
