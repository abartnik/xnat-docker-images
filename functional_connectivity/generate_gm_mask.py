import os
import begin


def gm_mask(acpc, output):
    """ Select only the 86 brain regions as defined in the freesurfer atlas
        that we are interested in studying brain activity in
    """

    segments = [8, 17, 18, 26, 28, 47, 58, 60]
    for i in range(10,14):
        segments.append(i)
    for i in range(49,55):
        segments.append(i)

    for i in range(1001,1036):
        if i != 1004:
            segments.append(i)

    for i in range(2001,2036):
        if i != 2004:
            segments.append(i)

    threshold_additon_cmd = "fslmaths "
    for seg in segments:
        if segments.index(seg) == 0:
            threshold_additon_cmd += "/tmp/GMmask" + str(seg) + ".nii.gz "
        else:
            threshold_additon_cmd += "-add /tmp/GMmask" + str(seg) + ".nii.gz "

        os.system(
            "fslmaths " + acpc + " -thr " + str(seg - 0.5) +
            " -uthr " + str(seg + 0.5) + " /tmp/GMmask" + str(seg) + ".nii.gz"
        )

    threshold_additon_cmd += " /tmp/GMmask.nii.gz"
    os.system(threshold_additon_cmd)
    
    for index in segments:
        temp_img = "GMmask" + str(index) + ".nii.gz"
        os.remove(f"/tmp/GMmask{str(index)}.nii.gz")

    os.rename("/tmp/GMmask.nii.gz", output)

@begin.start
def main(acpc, output="/tmp/GMmask.nii.gz"):

    gm_mask(acpc, output)
