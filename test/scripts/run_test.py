#!/usr/bin/env python

import docker
import begin
import os


PASSWD = os.environ['XNAT_PASSWORD']

@begin.start
def main(username, subjectID):

    docker_cmd = f'python test.py {username} {PASSWD} {subjectID}'
    client = docker.from_env()
    containers = client.containers
    result = containers.run('crania/test', docker_cmd)
    print(result)

