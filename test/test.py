import xnat
import begin


@begin.start
def main(username, passwd, subjectID):

    with xnat.connect("http://donut.bnac.net:8081", user=username, password=passwd) as session:

        cbi = session.projects['CBI']
        subject = cbi.subjects[subjectID]

        if len(subject.experiments.values()) > 0:
            for experiment in subject.experiments.values():
                experiment_id = experiment.data['dcmPatientId']
                scans = subject.experiments[experiment_id].scans

                if len(scans.values()) > 0:
                    for scan in scans.values():
                        print(scan.read_dicom())

