#!/bin/bash

XNAT_HOST=$1
USERNAME=$2
PASSWORD=$3

/bin/download_t1.py $XNAT_HOST $USERNAME $PASSWORD
dcm2niix -z y -d 9 /input/DICOM

find /t1 -type f -name "*.nii" -exec gzip {} \;
find /input -type f -name "*.nii" -exec gzip {} \;

find /t1 -type f -name "*.nii.gz" -exec cp -v {} /bids/sub-01/anat/sub-01_T1w.nii.gz \;
find /input -type f -name "*.nii.gz" -exec cp -v {} /bids/sub-01/func/sub-01_task-rest_bold.nii.gz \;

cpac run --pipe_config /cpac/pipeline.yml /cpac/data_config_XNAT-CPAC.yml
