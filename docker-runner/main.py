from typing import List
from fastapi import FastAPI, File, UploadFile, BackgroundTasks
from pydantic import BaseModel
from typing import Optional, List
# from fastapi.responses import HTMLResponse, FileResponse
import os
import shutil
import tarfile
import docker


client = docker.from_env()
xnat_username = os.environ['XNAT_USER']
xnat_password = os.environ['XNAT_PASSWORD']

#runner_path = "/shared/nonrestricted/cbi-development/docker/crania/docker-runner/nemo2_runner"
runner_path = "/tmp/nemo2_runner"
if not os.path.isdir(runner_path):
    os.mkdir(runner_path)

# def make_tarball(source, output):
#     with tarfile.open(output, 'w:gz') as tar:
#         tar.add(source, arcname="")
#     return output

def run_nemo2(input_dir, output_dir, run_status, exam_id, cmd):
    client.containers.run("registry.bnac.net/crania/nemo:2.1", cmd,
            volumes= {
                input_dir: {
                    'bind': '/FLL_HIGH',
                    'mode': 'rw'
                    },
                output_dir: {
                    'bind': '/output',
                    'mode': 'rw'
                    },
                '/shared/nonrestricted/nemo2/data': {
                    'bind': '/data',
                    'mode': 'rw'
                    }
                },
            cpuset_cpus="4",
            remove=True)
    run_status[exam_id] = "complete"


app = FastAPI()
app.runs = []


@app.post("/nemo/{exam_id}/{scan_id}/{xnat}")
async def nemo(exam_id: str,
               scan_id: str,
               xnat: str,
               background_tasks: BackgroundTasks, 
               fll: UploadFile = File(...)): 
    print(exam_id)
    run_status = {exam_id: "incomplete"}
    app.runs.append(run_status)
    
    if not os.path.isdir(os.path.join(runner_path, exam_id)):
        os.mkdir(os.path.join(runner_path, exam_id))
    output_dir = os.path.join(runner_path, exam_id, "output")

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    input_dir = os.path.join(runner_path, exam_id, "input")
    if not os.path.isdir(input_dir):
        os.mkdir(input_dir)
    
    filename = f'{input_dir}/fll_high.nii.gz'
    content = await fll.read()
    with open(filename, 'wb') as file_contents:
        print(type(content))
        file_contents.write(content)

    if 'internal' in xnat.lower():
        xnat_host = "http://donut.bnac.net:8082"
    elif 'external' in xnat.lower():
        xnat_host = "http://xnat.cbi.bnac.net:8080"
    else:
        return({"invadlidXnatLocation": xnat})
    cmd = f"bash /bin/nemo2.sh {xnat_host} {xnat_username} {xnat_password} {exam_id} {scan_id}"
    background_tasks.add_task(run_nemo2, input_dir, output_dir, run_status, exam_id, cmd)
    return({"launched_nemo": exam_id})

@app.get("/runs/trim")
async def trim_runs_list():
    """
    Trim the list of runs to free up memory
    """
    for i in range(len(app.runs) - 10):
        app.runs.pop(0)
    return({"Runs": app.runs})

@app.get("/runs")
async def get_runs():
    """
    Returns NeMo runs
    """
    return({"Runs": app.runs})

@app.get("/runs/clean")
async def clean_runs_dir():
    """
    Remove all results directories and tarballs in runner_path
    """
    for subdir in os.listdir(runner_path):
        full_subdir_path = f"{runner_path}/{subdir}"
        shutil.rmtree(full_subdir_path)
    return("Cleaned up runs directory")
