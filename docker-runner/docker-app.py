from flask import Flask, request, make_response, send_file
from flask_restful import Resource, Api, reqparse
import docker
import tarfile
import werkzeug
import os
import shutil


app = Flask(__name__)
api = Api(app)

client = docker.from_env()

jobs = {}
MAC_ADDRESS = "f4:8e:38:c0:b5:55"

def make_tarball(source, output):
    with tarfile.open(output, 'w:gz') as tar:
        tar.add(source, arcname="")
    return output

class Test(Resource):
    def get(self):
        print("Testing...\n")
        test = 'input'
        response = make_tarball(test, 'output.tar.gz')
        return send_file(response)

class SubmitJob(Resource):

    def get(self, job_id):
        return {job_id: jobs[job_id]}

    def put(self, job_id):

        if not os.path.isdir(f'/tmp/{job_id}'):
            os.mkdir(f'/tmp/{job_id}')
            os.mkdir(f'/tmp/{job_id}/input')
            os.mkdir(f'/tmp/{job_id}/output')
        else:
            shutil.rmtree(f'/tmp/{job_id}')
            os.mkdir(f'/tmp/{job_id}')
            os.mkdir(f'/tmp/{job_id}/input')
            os.mkdir(f'/tmp/{job_id}/output')

        parser = reqparse.RequestParser()
        parser.add_argument('image', 
                            type=str, 
                            required=True)
        parser.add_argument('command', 
                            type=str, 
                            required=True)
        args = parser.parse_args()

        image = args['image']
        command = args['command']

        input_files = request.files.getlist("inputs[]")
        for input_file in input_files:
            input_file.save(f'/tmp/{job_id}/input/{input_file.filename}')

        jobs[job_id] = {'image': image,
                        'command': command}

        client.containers.run(image, f"bash -c '{command}' ",
                volumes= {
                    f'/tmp/{job_id}/input': {
                        'bind': '/input',
                        'mode': 'rw'
                        },
                    f'/tmp/{job_id}/output': {
                        'bind': '/output',
                        'mode': 'rw'
                        }
                    },
                remove=True,
                mac_address=MAC_ADDRESS)

        response = make_tarball(os.path.join(f'/tmp/{job_id}/output', '/output.tar.gz'))

        return send_file(response)

api.add_resource(Test, '/')
api.add_resource(SubmitJob, '/<string:job_id>')

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
