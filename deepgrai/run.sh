#!/bin/bash

INPUT=$1
XNAT_HOST=$2
XNAT_USER=$3
XNAT_PASSWORD=$4

echo "Input: $INPUT"
ls $INPUT
echo " "

find $INPUT -type f -name "*nii*" -exec cp -v {} /flair.nii.gz \;
python3 /deepgrai/run.py flair.nii.gz
python3 /bin/qc.py stage3_input.nii.gz output_segmentation.nii.gz

thalalmic_volume="$(cat thalamic_volume.txt)"
echo "$thalalmic_volume"
mv -v thalamic_volume.txt /thal_vol/thalamic_volume.txt
mv -v qcimg.png /thal_vol/qcimg.png

# python /bin/z_score.py "$XNAT_HOST" "$XNAT_USER" "$XNAT_PASSWORD" "$thalalmic_volume"
mv -v z-score.txt /zscore
