import nibabel as nb
import nilearn
from nilearn import plotting
import begin


@begin.start
def main(crop_file, thal_file):
    crop = nb.load(crop_file)
    thal = nb.load(thal_file)
    thal_bin = nilearn.image.binarize_img(thal)

    qc = plotting.plot_anat(crop, 
                    title="Thalamus",
                    draw_cross=False,
                    black_bg=True)
    qc.add_contours(thal_bin, filled=False, colors='r')
    qc.savefig("qcimg.png")
