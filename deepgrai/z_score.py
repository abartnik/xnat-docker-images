#!/usr/bin/env python

import begin
import xnat
import numpy as np


@begin.start
def main(xnat_host, username, password, thal_vol):

    thalamic_volumes = []
    with xnat.connect(xnat_host, user=username, password=password) as session:
        
        print("Connected to CBI XNAT")
        
        projects = [project for project in session.projects]
        
        for xnat_id, subject in session.projects['FLAIR'].subjects.items():
            for experiment in subject.experiments.values():
                for scan in experiment.scans.values():
                    try:
                        thalamic_volume_resource = scan.resources.data_maps[1]['THALAMIC_VOLUME']

                        for file in scan.files.items():

                            if 'thalamic_volume' in file[0]:
                                with file[-1].open() as thalamic_volume_value:
                                    thalamic_volume = thalamic_volume_value.read().decode('utf-8')

                                    thalamic_volume_resource.data['content'] = thalamic_volume
                                    thalamic_volumes.append(float(thalamic_volume))

                    except:
                        print("Something wonky for scan")
                        pass
                    
    avg_thal_vol = np.mean(thalamic_volumes)
    std_thal_vol = np.std(thalamic_volumes)
    z = ( float(thal_vol) - avg_thal_vol ) / std_thal_vol
    with open("z-score.txt", "w") as z_file:
        z_file.write(str(z))
