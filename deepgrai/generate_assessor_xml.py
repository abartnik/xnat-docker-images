import begin
import xnat
import csv
from datetime import datetime
from lxml import etree as ET
from lxml.builder import ElementMaker


def get_deepgrai_stats(xnat_host, username, password):
    with xnat.connect(xnat_host, user=username, password=password) as session:
        pass

def ns(namespace, tag):
    return "{%s}%s"%(nsdict[namespace], tag)

nsdict = {
    'xnat': 'http://nrg.wustl.edu/xnat',
    'xs': 'http://www.w3.org/2001/XMLSchema-instance',
    'crania': 'http://xnat.cbi.bnac.net/crania'
}

def schemaLoc(namespace, xnat_host):
    return "{0} {2}/schemas/{1}/{1}.xsd".format(nsdict[namespace], namespace, xnat_host)

def generate_assessor_xml(deepgrai, xnat_host, project_name, session_id, session_label, scan_id):

    now = datetime.today()
    assessorId = '{}_thalVol'.format(session_id, scan_id)
    assessorLabel = '{}_thalVol'.format(session_label, scan_id)
    date = now.strftime('%Y-%m-%d')

    assessorTitleAttributesDict = {
        'ID': assessorId,
        'label': assessorLabel,
        'project': project_name,
        ns('xs','schemaLocation'): ' '.join(schemaLoc(namespace, xnat_host) for namespace in ('xnat', 'crania'))
    }

    E = ElementMaker(namespace=nsdict['crania'], nsmap=nsdict)
    assessorXML = E('ThalamicVolume', assessorTitleAttributesDict,
        E(ns('xnat', 'date'), date),
        E(ns('xnat', 'imageSession_ID'), session_id),
        E(ns('crania', 'thalamicVolume'), deepgrai['thalamicVolume']),
        E(ns('crania', 'thalamiczScore'), deepgrai['thalamiczScore'])
    )

    xml_contents = ET.tostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True)
    with open('/output/thalamicVolume.xml', 'wb') as xml_file:
        xml_file.write(xml_contents)

@begin.start
def main(xnat_host,  
        username, 
        password,
        project_name,
        session_id,
        session_label,
        scan_id):

    with open('/input/THALAMIC_VOLUME/thalamic_volume.txt', 'r') as thal_vol_file:
        thal_vol = str(thal_vol_file.readline())

    with open('/input/THALAMIC_ZSCORE/z-score.txt', 'r') as z_file:
        z_score = str(z_file.readline())

    deepgrai = {'thalamicVolume': thal_vol, 
        'thalamiczScore': z_score}

    with open('/output/thalamicVolume.csv', 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=list(deepgrai.keys()))
        writer.writeheader()
        writer.writerow(deepgrai)

    generate_assessor_xml(deepgrai, xnat_host, project_name, session_id, session_label, scan_id)
