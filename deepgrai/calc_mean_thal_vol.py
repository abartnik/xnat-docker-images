#!/usr/bin/env python

from glob import glob
from numpy import mean, std
import begin


@begin.start
def main(project):

    thalamic_volumes = []
    path = project + "/**/SCANS/**/THALAMIC_VOLUME/thalamic_volume.txt"
    files = glob(project + "/**/thalamic_volume.txt", recursive=True)

    for file in files:
        with open(file, 'r') as thal_vol_file:
            thalamic_volume = float(thal_vol_file.readline())
            thalamic_volumes.append(thalamic_volume)

    avg_thal_vol = mean(thalamic_volumes)
    std_thal_vol = std(thalamic_volumes)

    with open("avg_thal_vol.txt", "w") as avg_thal_vol_file:
        avg_thal_vol_file.write(str(avg_thal_vol))

    with open("std_thal_vol.txt", "w") as std_thal_vol_file:
        std_thal_vol_file.write(str(std_thal_vol))
