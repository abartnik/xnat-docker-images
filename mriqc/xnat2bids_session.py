#!/usr/bin/env python3

import os
import sys
import shutil
import subprocess
from glob import glob
import numpy as np
from scan_classifier.scan_classifier import read_dcm_header, read_bids_sidecar, format_input_for_model, predict_scan_type, model
from scan_classifier.ScanType import ScanType


try:
    sub_id = sys.argv[1]
except:
    print("No subject ID specified, exiting...")
    exit(1)

def get_bids_path(scan, scan_type, scan_id, sub_id) -> str:

    if scan_type == ScanType.MPRAGE:
        bids_path = f"/bids/sub-{sub_id}/anat/sub-{sub_id}_run-{scan_id}_T1w"
    elif scan_type == ScanType.SPGR:
        bids_path = f"/bids/sub-{sub_id}/anat/sub-{sub_id}_run-{scan_id}_T1w"
    elif scan_type == ScanType.FLAIR:
        bids_path = f"/bids/sub-{sub_id}/anat/sub-{sub_id}_acq-FLAIR_run-{scan_id}_T2w"
    elif scan_type == ScanType.rsfmri:
        bids_path = f"/bids/sub-{sub_id}/func/sub-{sub_id}_run-{scan_id}_task-rest_bold"
    elif scan_type == ScanType.DWI:
        bids_path = f"/bids/sub-{sub_id}/dwi/sub-{sub_id}_run-{scan_id}_dwi"
    elif scan_type == ScanType.PD:
        bids_path = f"/bids/sub-{sub_id}/anat/sub-{sub_id}_run-{scan_id}_PDw"
    elif scan_type == ScanType.T2star:
        bids_path = f"/bids/sub-{sub_id}/anat/sub-{sub_id}_run-{scan_id}_T2starw"
    elif scan_type == ScanType.FSE:
        bids_path = f"/bids/sub-{sub_id}/anat/sub-{sub_id}_run-{scan_id}_T2w"
    else:
        print(scan, scan_type, scan_id, " No BIDS path")
        bids_path = ""
    
    return(bids_path)

nifti_path = "/tmp/niftis"
niftis = glob(f"{nifti_path}/*nii.gz")
sidecars = glob(f"{nifti_path}/*json")
bvecs = glob(f"{nifti_path}/*bvec")
bvals = glob(f"{nifti_path}/*bval")

scans = {}
for sidecar in sidecars:
    name = sidecar.split('.json')[0].split('/')[-1]
    xnat_path = glob(f"/input/SCANS/**/{name}.json", recursive=True)[0]
    scan_id = xnat_path.split('/')[3]
    scans[name] = {"nifti": "",
                   "sidecar": sidecar,
                   "bvec": "",
                   "bval": "",
                   "scan_id": scan_id}
    
    nifti = [ nifti for nifti in niftis if name in nifti ][0]
    try:
        bvec = [ bvec for bvec in bvecs if name in bvec ][0]
        bval = [ bval for bval in bvals if name in bval ][0]
    except IndexError:
        bvec = None
        bval = None
    
    scans[name].update({"nifti": nifti})
    scans[name].update({"bvec": bvec})
    scans[name].update({"bval": bval})

for scan in scans:
    
    if scans[scan]['bvec'] != None and scans[scan]['bval'] != None:
        scan_type = ScanType.DWI
    else:
        metadata = read_bids_sidecar(scans[scan]['sidecar'])
        model_inputs = format_input_for_model(metadata)
        scan_type = predict_scan_type(model, model_inputs)
    
    bids_path = get_bids_path(scan, scan_type, scans[scan]['scan_id'], sub_id)
    if scan_type == ScanType.rsfmri:
        fslinfo = subprocess.check_output( ["fslinfo", scans[scan]['nifti']] ).decode("utf-8").split('\n')
        dim4 = int([ info for info in fslinfo if 'dim4' in info ][0].split(' ')[-1])
        if dim4 < 3:
            scan_type = ScanType.fmap
            scan_id = scan.split("_")[-1]
            try:
                int(scan_id)
            except ValueError:
                continue
            with open(scans[scan]['sidecar'], 'r') as sidecar_file:
                import json
                metadata = json.load(sidecar_file)
            try:
                pe_dir = metadata['PhaseEncodingDirection']
                bids_path = f"/bids/sub-{sub_id}/fmap/sub-{sub_id}_dir-{pe_dir}_run-{scan_id}_epi"
            except KeyError:
                continue
    
    shutil.copyfile(scans[scan]['nifti'], bids_path + ".nii.gz")
    shutil.copyfile(scans[scan]['sidecar'], bids_path + ".json")
    
    if 'dwi' in scan_type.name:
        shutil.copyfile(scans[scan]['bvec'], bids_path + ".bvec")
        shutil.copyfile(scans[scan]['bval'], bids_path + ".bval")
