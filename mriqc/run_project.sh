#!/bin/bash

for exam_id in /input/* ; do
    rsync -av $exam_id/RESOURCES/BIDS/ /bids --progress
done

mriqc /bids/ /output/ participant --no-sub --no-track
mriqc /bids/ /output group --no-sub --no-track
