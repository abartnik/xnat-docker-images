#!/bin/bash

session_id=$1
if [ ${#session_id} -lt 1 ]; then
    session_id="01"
fi
mv /bids/sub- "/bids/sub-$session_id"
echo "sub-$session_id" >> /bids/participant.tsv

mkdir /tmp/niftis
if [[ ! -n $(find /input -type d -name "*NIFTI") ]]; then
    dcm2niix -z y -b y -f %d_%x_%z_%s -o /tmp/niftis /input
else
    find /input -type f -wholename "**/NIFTI/**nii.gz" -exec cp -v {} /tmp/niftis \;
    find /input -type f -wholename "**/NIFTI/**json" -exec cp -v {} /tmp/niftis \;
fi

xnat2bids_session.py "$session_id"

tree /bids

mriqc /bids /output participant --no-sub
