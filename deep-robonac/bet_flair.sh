#!/bin/bash
input=$1
output=$2
threshold=$3

# Add check for .gz ending and gzip if necessary, just in case
find "$input"/NIFTI -type f -name '*nii*' -exec cp -v {} /tmp \;
find /tmp -type f -name "*.nii" -exec gzip {} \;

find /tmp -type f -name '*nii.gz' -exec cp -v {} /work/flair.nii.gz \;

bet2 /work/flair.nii.gz /output/flair -n -m -f "$threshold"
find "$output" -type f -name "*nii.gz" -exec mv -v {} "$output/mask.nii.gz" \;