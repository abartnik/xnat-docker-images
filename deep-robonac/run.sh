#!/bin/bash

mkdir /tmp/flair /tmp/mask

find /input/NIFTI -type f -name "*nii*" -exec cp -v {} /tmp/flair \;
find /input/MASK -type f -name "*nii*" -exec cp -v {} /tmp/mask \;

find /tmp -type f -name "*nii*" -exec gzip {} \;

find /tmp/flair -type f -name "*nii.gz" -exec cp -v {} /work/flair.nii.gz \;
find /tmp/mask -type f -name "*nii.gz" -exec cp -v {} /work/flair_mask.nii.gz \;

mkdir -p /work/fll
python3 /bin/deep_robonac_runner.py /work/flair.nii.gz /work/fll/dr_fllp_raw.nii.gz
fslmaths /work/fll/dr_fllp_raw.nii.gz -mas /work/flair_mask.nii.gz /work/fll/dr_fllp.nii.gz 
double_threshold -l 0.5 -u 0.5 /work/fll/dr_fllp.nii.gz /work/fll/dr_fllp_dt.nii.gz
remove_small_blobs -t 14.137 /work/fll/dr_fllp_dt.nii.gz /output/fll.nii.gz