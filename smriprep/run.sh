#!/bin/bash

n_cores=$(expr $(lscpu -p | grep -v "#" | wc -l) / 4)

mkdir /tmp/t1

## Move/convert to nifti staging folders
if [ -d "/t1/NIFTI" ]; then
    cp -v /t1/NIFTI/* /tmp/t1
else
    dcm2niix -z y -d 9 -o /tmp/t1 /t1/DICOM
fi

find /tmp/t1 -type f -name "*nii" -exec gzip {} \;
find /tmp/t1 -type f -name "*.nii.gz" -exec mv -v {} /bids/sub-01/anat/sub-01_T1w.nii.gz \;
find /tmp/t1 -type f -name "*.json" -exec mv -v {} /bids/sub-01/anat/sub-01_T1w.json \;

tree /bids

smriprep /bids /output participant \
    --no-submm-recon \
    --fs-license-file /opt/freesurfer/license.txt \
    --output-spaces MNI152NLin6Asym MNI152NLin2009cAsym \
    --n_cpus $n_cores \
    --notrack 

find /output -type l -exec rm {} \;
