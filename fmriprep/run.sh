#!/bin/bash

session_id=$1
echo "$session_id"
n_cores=$(expr $(lscpu -p | grep -v "#" | wc -l) / 4)

mkdir /tmp/t1 /tmp/fmri /tmp/fmap mkdkir /tmp/smriprep

rsync -a /anat/smriprep /tmp/smriprep
rsync -a /anat/freesurfer /tmp/smriprep

## Move/convert to nifti staging folders
if [ -d "/t1/NIFTI" ]; then
    cp -v /t1/NIFTI/* /tmp/t1
else
    dcm2niix -z y -d 9 -o /tmp/t1 /t1/DICOM
fi

if [ -d "/fmri/NIFTI" ]; then
    cp -v /fmri/NIFTI/* /tmp/fmri
else
    dcm2niix -z y -d 9 -o /tmp/fmri /fmri/DICOM
fi

if [ -d "/fmap/NIFTI" ]; then
    cp -v /fmap/NIFTI/* /tmp/fmap
else
    if [ -d "/fmap/DICOM" ]; then
        dcm2niix -z y -d 9 -o /tmp/fmap /fmap/DICOM
    else
        echo "No fieldmap"
    fi
fi

echo "Checking fieldmap/EPI blips..."
python /bin/check_epi_blips.py "$session_id"
if [ -d "/tmp/epi_reversed" ]; then
    dcm2niix -z y -o /tmp/epi_reversed -f "epi_blips_reversed" -d 9 /tmp/epi_reversed
fi
if [ -d "/tmp/epi_blips" ]; then
    dcm2niix -z y -o /tmp/epi_blips -f "epi_blips" -d 9 /tmp/epi_blips
fi
echo "Got EPI blips"
echo ""

find /tmp/t1 -type f -name "*nii" -exec gzip {} \;
find /tmp/t1 -type f -name "*.nii.gz" -exec mv -v {} /bids/sub-01/anat/sub-01_T1w.nii.gz \;
find /tmp/t1 -type f -name "*.json" -exec mv -v {} /bids/sub-01/anat/sub-01_T1w.json \;

find /tmp/fmri -type f -name "*nii" -exec gzip {} \;
# num_vols=$(fslstats /tmp/fmri/*.nii.gz -w | awk '{print $8}')
find /tmp/fmri -type f -name "*.nii.gz" -exec fslroi {} /bids/sub-01/func/sub-01_task-rest_bold.nii.gz 2 -1 \;
find /tmp/fmri -type f -name "*.json" -exec mv -v {} /bids/sub-01/func/sub-01_task-rest_bold.json \;

echo "Moving EPI blps into BIDS folder"
if [ -f "/tmp/epi_reversed/epi_blips_reversed.nii.gz" ]; then
    mkdir -p /bids/sub-01/fmap/
fi
find /tmp/epi_reversed -type f -name "*nii" -exec gzip {} \;
find /tmp/epi_reversed -type f -name "*.nii.gz" -exec mv {} /bids/sub-01/fmap/sub-01_dir-AP_epi.nii.gz \;
find /tmp/epi_reversed -type f -name "*.json" -exec mv {} /bids/sub-01/fmap/sub-01_dir-AP_epi.json \;

find /tmp/epi_blips -type f -name "*nii" -exec gzip {} \;
find /tmp/epi_blips -type f -name "*.nii.gz" -exec mv {} /bids/sub-01/fmap/sub-01_dir-PA_epi.nii.gz \;
find /tmp/epi_blips -type f -name "*.json" -exec mv {} /bids/sub-01/fmap/sub-01_dir-PA_epi.json \;
python /bin/edit_epi_sidecars.py
echo ""

tree /bids

## Run fmriprep
if [ -f "/bids/sub-01/fmap/sub-01_dir-AP_epi.nii.gz" ]; then
    /opt/conda/bin/fmriprep /bids /output participant \
        -w /work \
        --no-submm-recon \
        --ignore sbref t2w flair \
        --fs-license-file /opt/freesurfer/license.txt \
        --use-aroma \
        --resource-monitor \
        --n_cpus $n_cores \
        --notrack
else
    /opt/conda/bin/fmriprep /bids /output participant \
        -w /work \
        --use-syn-sdc \
        --no-submm-recon \
        --ignore sbref t2w flair \
        --fs-license-file /opt/freesurfer/license.txt \
        --use-aroma \
        --resource-monitor \
        --n_cpus $n_cores \
        --notrack
fi

rm -rf /output/sourcedata/freesurfer/fsaverage
