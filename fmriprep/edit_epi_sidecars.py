import json


epi_blips = "/bids/sub-01/fmap/sub-01_dir-PA_epi.json"
epi_reverse = "/bids/sub-01/fmap/sub-01_dir-AP_epi.json"
intended_for = "func/sub-01_task-rest_bold.nii.gz"
func = "/bids/sub-01/func/sub-01_task-rest_bold.json"

###EPI AP direction###
with open(epi_reverse, 'r') as infile:
    json_dict=json.load(infile)
json_dict['IntendedFor'] = [intended_for]
json_dict['PhaseEncodingDirection'] = 'j-'
with open(epi_reverse, 'w') as outfile:
    json.dump(json_dict,outfile)

###EPI PA direction###
with open(epi_blips, 'r') as infile:
    json_dict=json.load(infile)
json_dict['IntendedFor'] = [intended_for]
json_dict['PhaseEncodingDirection'] = 'j'
with open(epi_blips, 'w') as outfile:
    json.dump(json_dict,outfile)

###Func files###
with open(func, 'r') as infile:
    json_dict=json.load(infile)
json_dict['PhaseEncodingDirection'] = 'j'
with open(func, 'w') as outfile:
    json.dump(json_dict,outfile)
