#!/usr/local/miniconda/bin/python

import os
import xnat
import begin
from pydicom import dcmread
from zipfile import ZipFile
from glob import glob


def get_subjectID():

    for dcm in os.listdir('/input/DICOM'):
        dcm_file = os.path.join('/input/DICOM', dcm)
        path_id = ""
        
        try:
            pat_id = dcmread(dcm_file).PatientID
            break
        except:
            pass

    return(pat_id)

@begin.start
def main(xnat_host, username, password):

    with xnat.connect(xnat_host, user=username, password=password) as session:

        subjectID = get_subjectID()

        for xnat_id, subject in session.projects['MPRAGE'].subjects.items():
            if subjectID == subject.label:
                for experiment in subject.experiments.values():
                    for scan in experiment.scans.values():                    
                        scan.download('/tmp/mprage.zip')

    with ZipFile('/tmp/mprage.zip', 'r') as download:
        download.extractall('/tmp')

    dcmdir = glob("/tmp/**/DICOM", recursive=True)
    os.system(f"dcm2niix -o /t1 -z y -d 9 /tmp/")
