import begin
import xnat
from zipfile import ZipFile
import os
import numpy as np


@begin.start
def main(xnat_url, xnat_user, xnat_password, session_id):
    print(session_id)
    
    with xnat.connect(xnat_url, user = xnat_user, password = xnat_password) as session:
    
        epi = []
        epi_reverse = []
        rsfmri = ""
        exam = session.experiments[session_id]
        
        for series_number, scan in exam.scans.items():
            if 'rsfmri' in scan.type:
                rsfmri = int(series_number)
            elif 'epi' in scan.type.lower():
                if 'reverse' in scan.type.lower():
                    epi_reverse.append(int(series_number))
                else:
                    epi.append(int(series_number))
                    
        if len(epi) > 0 and len(epi_reverse) > 0:
            epi_series_number = np.max([ sn for sn in epi if sn < rsfmri ])
            epi_reverse_series_number = np.max([ sn for sn in epi_reverse if sn < rsfmri ])
            
            epi_scan = session.experiments[session_id].scans[str(epi_series_number)]
            epi_scan.download('/tmp/epi_blips.zip', verbose=True)
            if not os.path.isdir('/tmp/epi_blips/'):
                os.mkdir('/tmp/epi_blips')
            with ZipFile('/tmp/epi_blips.zip', 'r') as download:
                download.extractall('/tmp/epi_blips')
            
            epi_reverse_scan = session.experiments[session_id].scans[str(epi_reverse_series_number)]
            epi_reverse_scan.download('/tmp/epi_reversed.zip', verbose=True)
            if not os.path.isdir('/tmp/epi_reversed/'):
                os.mkdir('/tmp/epi_reversed')
            with ZipFile('/tmp/epi_reversed.zip', 'r') as download:
                download.extractall('/tmp/epi_reversed')
