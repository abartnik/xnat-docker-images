#!/bin/bash

host=$1
port=$2
exam_id=$3
scan_id=$4
xnat_location=$5
runner_url=$(echo "$host" | cut -d':' -f1,2 )
runner_url="$runner_url:$port"
# cmd_id=$(date +"%Y%m%d-%H%M%S")

curl -L "$runner_url/nemo/$exam_id/$scan_id/$xnat_location" \
  -F "fll=@/fll/fll_high.nii.gz" 
