#!/bin/bash

input=$1
nifti=$2

# find "$input" -type f -name "*nii" -exec cp -v {} /work/flair.nii \;
# find /work -type f -name "*nii" -exec gzip {} \;
# find "$input" -type f -name "*nii.gz" -exec cp -v {} /work/flair.nii.gz \;
if [[ "$nifti" == "nifti" ]]; then
    cp -v /input/FLAIR_MNI/flair_std_n4.nii.gz /work/flair.nii.gz
else
    find "$input" -type f -name "*flair_std_n4.nii.gz" -exec cp -v {} /work/flair.nii.gz \;
fi

/algorithm/neurostream -o /output /work/flair.nii.gz
